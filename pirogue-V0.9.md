Ce document décrit les éléments de base de collaboration pour la pirogue appelée :

**NOM DU PROJET**

## Intention

L’intention/mission de cette pirogue est de : 

1. INTENTION 1
2. INTENTION 2
3. ETC...

## Embarquement

L'inclusion de nouveaux membres au projet se fera sur cooptation d'au moins **70%** des membres existants

## Débarquement

### Par décision du membre

Si un membre de l’association souhaite quitter l’association, ce n’est pas un problème. Mais signer cette charte vous engage à organiser un appel avec au moins un membre de l'Equipage pour discuter des raisons de son départ et s’assurer de la transmission des tâches dont il était responsable.

### Par décision de l'Equipage

Si un membre ne respecte pas les engagements définis dans ce document, un représentant du équipage prend contact avec le membre concerné pour discuter de la situation.

Si cette situation devait perdurer, il sera convoqué pour un entretien avec au moins **XX** membres de l'Equipage. Le compte rendu de cet entretien sera ensuite communiqué au reste de l'Equipage. Cet entretien permettra au Collectif de statuer pour définir si la charte doit évoluer, ou s'il est nécessaire d'appliquer une sanction pouvant aller jusqu'à l'exclusion.


## Résolution de conflits

Lorsqu’une tension ou un conflit émerge, la procédure utilisée sera celle définie dans le manuel de coopération de Loomio : https://loomio.coop/conflict_resolution.html

## Licence appliquée

Tous les documents et livrables produits par cette pirogue, y compris ce document sont partagés sous [licence CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)

## Outils numériques utilisés

NOM DE L'OUTIL , LIEN , SUPPORT
(exemple : Gogocarto : https://gogocarto.fr/projects , support technique : https://chat.lescommuns.org/channel/gogocarto )

## Financement

Les financements pour ce projet proviendront d’un appel de fonds réalisé avec www.Leetchi.com

### L’argent sera utilisé pour :

### La répartition du budget sera définie par :
### Le mode de décision utilisé sera :

## Cadre de sécurité

Le cadre de sécurité appliqué est celui de [l’Université du Nous](http://universite-du-nous.org/wp-content/uploads/2012/02/cadre-de-securite-adn.jpg)

## Valeurs communes

Nos valeurs conditionnent nos modes de collaboration. 
Vous trouverez ci-dessous celles qui nous animent et qu’appliquent les collaborateurs du projet. 
Nous les voulons sereines et joyeuses pour soi et pour l’autre.

* Souveraineté : je sais qui je suis et ce que j’amène pour le groupe et ses membres. Je prends soin de moi, je sais exprimer mes besoins et mes limites. Les membres de l’association ont confiance en mes capacités à le faire.

* Engagement : je m’engage à faire ce que je me suis engagé.e à faire. Je suis responsable de l’avancement des sujets que j’entreprends. Je demande de l’aide lorsque je ne peux pas tenir mon engagement et m’arrange pour que ma mission soit réalisée.

* Partage et transparence : je suis à l’écoute de ce que font les autres pour enrichir mon expérience, en faire quelque chose d’utile et ne pas doubler le travail. Je partage au groupe ce que je fais et ce que font les autres pour que tous les membres soient au même niveau d’information pour prendre les meilleures décisions.

* Humilité : je suis conscient.e de ma vulnérabilité et l’exprime si nécessaire. Je sais prendre du recul par rapport à mes émotions et travaille sur ma part d’ombre. Je me rappelle que les autres n’ont pas forcément tort et que le point de vue de l’autre m’est utile.

* Authenticité : je suis pleinement moi-même, je dis ce que je pense franchement pour moi et pour les autres, en respectant l’autre grâce à la méta-communication, à l’intelligence émotionnelle ou la CNV.

* Bienveillance : je suis attentif.ve à ce que l’autre ressent, à ses réactions lors de nos interactions et en dehors de nos échanges. L’épanouissement de chaque membre est central dans nos relations, c’est à tous les membres de prendre soin des autres.

* Inclusion et ouverture : la différence est perçue comme une chance et une force pour l'Equipage. Elle est valorisée dans nos interactions, notamment par la confrontation de nos idées pour enrichir nos idées et nos points de vue. 

* Convivialité : il est possible d’être sérieux et professionnel tout en s’amusant. Nous tenons à ce que la majorité des moments passés ensemble soient des bons moments.
      
* Soutien mutuel et confiance : la sécurité émotionnelle est un pilier majeur d’une bonne collaboration. Offrir des signes de confiance et se rendre disponible pour l’autre sont des clés pour créer un climat serein au sein de l’association

## Embarquement

En montant à bord de cette pirogue, je déclare adhérer au cadre défini ci-dessus.


* Date
* Organisme
* Nom, prénom
* Signature



















